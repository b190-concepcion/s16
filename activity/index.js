console.log("Hello World");

	let num1 = 25;
	let num2 = 5;

	let actualResult = num1 + num2;
	console.log("The result of num1 + num2 should be 30 ");
	console.log("Actual Result:");
	console.log(actualResult);

	let num3 = 156;
	let num4 = 44;
	let actualResult2 = num3 + num4;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(actualResult2);

	let num5 = 17;
	let num6 = 10;

	let actualResult3 = num5 - num6;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(actualResult3);
		

	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;

	let resultMinutesPerDay = minutesHour * hoursDay;
	let resultMinutes = resultMinutesPerDay * daysYear;
	console.log("There are " + resultMinutes + " minutes in a year");


	let tempCelsius = 132;
	let fahrenheit = (tempCelsius*1.8) +32;
	console.log(tempCelsius + " degrees Celsius when converted to Farenheit is "+ fahrenheit);


	let num7 = 165;
	let remainder = num7 % 8;
	console.log("The remainder of " + num7 + " divided by 8 is: " + remainder);

	let isDivisibleBy8 = remainder === 0;
	console.log("Is num7 divisible by 8?");
	console.log(isDivisibleBy8);

	
	let num8 = 348;
	let remainder2 = num8 % 4;

	console.log("The remainder of " + num8 + " divided by 4 is: " + remainder2)
	
	let isDivisibleBy4 = remainder2 === 0;
	console.log("Is num8 divisible by 4?");
	console.log(isDivisibleBy4);

